<?php
ini_set('auto_detect_line_endings', TRUE);

class Noticia7Migration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);


    //The defintion of the collumns. Keys are integers. values are array(field name, description).
    // $columns = array(
    //   0 => array('guid',        'Nid'),
    //   1 => array('published_csv', 'Published'),
    //   2 => array('status_csv',    'Status'),
    //   3 => array('categ_csv',     'Section'),
    //   4 => array('pretitle_csv',  'Antetitulo'),
    //   5 => array('title_csv',     'Title'),
    //   //6 => array('entradilla_csv','-'),
    //   6 => array('body_csv',      'Body'),
    //   7 => array('image_csv',     'Image'),
    //   //9 => array('link_csv',      '-'),
    //  8 => array('file1_csv',     'Fichero1')
    // );
    $columns = array(
      0 => array('guid',          'Nid'),
      1 => array('published_csv', 'Published'),
      2 => array('status_csv',    'Status'),
      3 => array('categ_csv',     'Section'),
      4 => array('pretitle_csv',  'Antetitulo'),
      5 => array('title_csv',     'Title'),
      6 => array('entradilla_csv','Summary'),
      7 => array('image_csv',     'Image'),
      8 => array('body_csv',      'Body'),
      9 => array('file1_csv',     'Fichero1'),
      10 => array('link_csv',     'Url')
    );

    //The Description of the import. This desription is shown on the Migrate GUI
    $this->description = t('Import of noticia 7 content.');

    //The Source of the import
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/bak/carga_t_mypactual_3.csv',
		       $columns, array('delimiter' => '~', 'header_rows' => 1, 'embedded_newlines' => TRUE));

    //The destination CCK (boundle)
    $this->destination = new MigrateDestinationNode('openpublish_article');

    //Source and destination relation for rollbacks
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'guid' => array('type' => 'int',
                           'length' => 10,
                           'not null' => TRUE,
                           'description' => 'Topic ID',
                          )
        ),
        MigrateDestinationNode::getKeySchema()
      );

    //Field mapping
    $this->addFieldMapping('field_autor')->defaultValue('medicosypacientes.com');

    $this->addFieldMapping('field_lugar')->defaultValue('');

    $this->addFieldMapping('created', 'published_csv');

    $this->addFieldMapping('status', 'status_csv')->defaultValue('1');

    $this->addFieldMapping('field_op_section_term', 'categ_csv')->callbacks(array($this, 'computeValue'));
    $this->addFieldMapping('field_op_section_term:source_type')->defaultValue('tid');

    $this->addFieldMapping('field_antetitulo', 'pretitle_csv')->callbacks('strip_tags');

    $this->addFieldMapping('title', 'title_csv')->callbacks('strip_tags');

    $this->addFieldMapping('field_entradilla', 'entradilla_csv');
    $this->addFieldMapping('field_entradilla:format', NULL, FALSE)->defaultValue('full_html');

    $this->addFieldMapping('body', 'body_csv');
    $this->addFieldMapping('body:format', NULL, FALSE)->defaultValue('full_html');

    $this->addFieldMapping('field_op_main_image', 'image_csv');
    $this->addFieldMapping('field_op_main_image:source_dir')->defaultValue(DRUPAL_ROOT . '/sites/default/files/originales_myp');
    $this->addFieldMapping('field_op_main_image:destination_file', 'image_csv');
    $this->addFieldMapping('field_op_main_image:file_replace')->defaultValue('FILE_EXISTS_REUSE');
    $this->addFieldMapping('field_op_main_image:preserve_files')->defaultValue(TRUE);

    $this->addFieldMapping('field_archivo', 'file1_csv')->separator(',');
    $this->addFieldMapping('field_archivo:destination_file', 'file1_csv')->separator(',');
    $this->addFieldMapping('field_archivo:source_dir')->defaultValue(DRUPAL_ROOT . '/sites/default/files/originales_myp');
    $this->addFieldMapping('field_archivo:file_replace')->defaultValue('FILE_EXISTS_REUSE');
    $this->addFieldMapping('field_archivo:preserve_files')->defaultValue(TRUE);

    $this->addFieldMapping('field_url', 'link_csv')->separator(',');

  }

  public function prepareRow($row) {
	  // Always include this fragment at the beginning of every prepareRow()
	  // implementation, so parent classes can ignore rows.
	  if (parent::prepareRow($row) === FALSE) {
	    return FALSE;
	  }

	  $row->created = strtotime($row->created);
	  return TRUE;
  }

  protected function statusValue($value) {
    return 1;
  }

  protected function computeValue($value) {
    if ($value == 'Organizaciones') {
    	return array(4,28);
    }
    if ($value == 'Actualidad') {
    	return array(3,28);
    }
    if ($value == 'omc') {
    	return array(1,28);
    }
    if ($value == 'TIC') {
    	return array(3,28);
    }
    if ($value == 'Multimedia') {
    	return array(7,28);
    }
    if ($value == 'pacientes') {
    	return array(5,28);
    }
    if ($value == 'Fundaciones OMC') {
    	return array(2,28);
    }

    return "28";
  }

}
