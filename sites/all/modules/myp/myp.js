(function ($) {
  Drupal.behaviors.myp_opinion_menu= {
    attach: function (context, settings) {
	var _   = window.location.search.split("=");
	var url = window.location.href;

	$('.menu a').removeClass("active");

	if (_[1] == '29' ){ //EDITORIAL
	    $('body').addClass('page-editorial');
	    $('.block-menu-opini-n .clearfix ul.menu li').removeClass('active-trail');
	    $('.block-menu-opini-n .clearfix ul.menu li a').removeClass('active-trail');
	    $(".block-menu-opini-n .clearfix ul.menu li").eq(1).children().eq(0).addClass("active-trail");
	}

	if (_[1] == '30' ){ //ARTICULOS
	    $('body').addClass('page-articulos');
	    $('.block-menu-opini-n .clearfix ul.menu li').removeClass('active-trail');
	    $('.block-menu-opini-n .clearfix ul.menu li a').removeClass('active-trail');
	    $(".block-menu-opini-n .clearfix ul.menu li").eq(2).children().eq(0).addClass("active-trail");
	}
	if (_[1] == '31' ){ //ANÁLISIS
	    $('body').addClass('page-analisis');
	}
	if (window.location.pathname.indexOf('opinion') > 0 && (jQuery.isArray( _ ) == false )) {
	    $('body').addClass('page-opinion');
	}

	if (url.indexOf('videos') > 0) {
	    $('.menu-mlid-9937').addClass("active-trail");
	}

	if (url.indexOf('imagenes') > 0) {
	    $('.menu-mlid-9938').addClass("active-trail");
	}

	if (url.indexOf('graficos') > 0) {
	    $('.menu-mlid-9943').addClass("active-trail");
	}
      }
  };
  Drupal.behaviors.myp_newsletter = {
    attach: function (context, settings) {
        if (location.href.indexOf('newsletter') >= 0 ) {
          $('#region-menu').hide();	
        }
    }
  };

})(jQuery);
