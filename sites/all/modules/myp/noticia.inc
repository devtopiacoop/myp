<?php

class NoticiaMigration extends Migration {
  public function __construct($arguments) {
    parent::__construct($arguments);

    //The defintion of the collumns. Keys are integers. values are array(field name, description).
    $columns = array(
      0 => array('guid_csv',      'Nid'),
      1 => array('status_csv',    'Status'),
      2 => array('title_csv',     'Title'),
      3 => array('body_csv',      'Body'),
      4 => array('published_csv', 'Published'),
      5 => array('filename_csv',  'Filename'),
      6 => array('filepath_csv',  'Filepath'),
    );

    //The Description of the import. This desription is shown on the Migrate GUI
    $this->description = t('Import of noticia content.');

    //The Source of the import
    $this->source = new MigrateSourceCSV(DRUPAL_ROOT . '/bak/noticias.csv', $columns, array('delimiter' => ',', 'header_rows' => 1));

    //The destination CCK (boundle)
    $this->destination = new MigrateDestinationNode('openpublish_article');

    //Source and destination relation for rollbacks
    $this->map = new MigrateSQLMap(
      $this->machineName,
      array(
        'guid_csv' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'alias' => 'import'
        )
      ),
      MigrateDestinationNode::getKeySchema()
    );

    //Field mapping
    $this->addFieldMapping('title', 'title_csv');
    //$this->addFieldMapping('body', 'body_csv')->arguments(array('format' => 'full_html'));
    $this->addFieldMapping('body', 'body_csv');
    $this->addFieldMapping('body:format', NULL, FALSE)->defaultValue('full_html');
    $this->addFieldMapping('status', 'status_csv');
    $this->addFieldMapping('created', 'published_csv');


	$this->addFieldMapping('field_archivo', 'filename_csv');
	$this->addFieldMapping('field_archivo:source_dir')->defaultValue('/var/www/html/openpublish/sites/default/files/prensa/adjuntos');
	$this->addFieldMapping('field_archivo:destination_file', 'filename_csv');
	$this->addFieldMapping('field_archivo:file_replace')->defaultValue('FILE_EXISTS_REUSE');
	$this->addFieldMapping('field_archivo:preserve_files')->defaultValue(TRUE);

	$this->addFieldMapping('field_op_section_term')->defaultValue('28');
	$this->addFieldMapping('field_op_section_term:source_type')->defaultValue('tid');
  }
}
