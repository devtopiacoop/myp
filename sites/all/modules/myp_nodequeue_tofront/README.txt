                                MYP NODEQUEUE TO FRONT
                                ::::::::::::::::::::::
                                ::::::: README :::::::
                                ::::::::::::::::::::::
                                MYP NODEQUEUE TO FRONT



INSTALACIÓN y CONFIGURACIÓN
===========================

[DESARROLLADORES]

- asegurarse que tanto la vista de portada como la de preportada tiene la relación con su queue correspondiente
y los elementos están ordenados por su posición en su queue

- asegurarse que el bloque de Enlaces de Administración contiene el link al queue de Pre-Portada

[TODOS]

1. activar el módulo
2. en admin/people/permissions conceder el permiso «Manipulate queues» al Editor
3. establecer una llamada al cron cada hora en admin/config/system/cron
4. configurar el proceso en admin/config/myp_nodequeue_tofront
5. aplausos!



USO
===

Los editores encontrarán un link en su Dashboard a la cola de Pre-Portada
Deben configurar sus elementos con la posición deseada.

La preportada puede previsualizarse en /preportada

En admin/config/myp_nodequeue_tofront los editores deben configurar si activar el mecanismo de publicación
automática, y qué días y a qué hora.

Este módulo lo que hace es pasar a portada los elementos de la cola de Pre-Portada los días indicados a la hora indicada.

En caso de no activarse este módulo, la portada depende completamente de la cola de Portada, la cual hay que
mantener actualizada en caso de cambio.
