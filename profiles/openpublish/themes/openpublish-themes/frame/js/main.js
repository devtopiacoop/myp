(function ($) {


// FUNCTIONS AND DRUPAL METHODS
// ----------------------------------------------------------------------------------------------------

Drupal.initStart = function(){
  // console.log("Ignition Sequence Start...");
  // console.log(Drupal);
};

//Image Caption Function
Drupal.imageCaption = function(){
  if($('div.field-op-main-image').length > 0 && $('div.field-op-caption').length > 0){
    $("div.field-op-caption").map(function () {
        var w = $(this).prev().find('img').width();
        //w = (w>150)?w:150;
        $(this).width(w);
        $(this).css('margin-right',20);
    });
  }
};

Drupal.manipulateDrupalHTML = function(){
  //Responsive Menu Button
  if($('nav.navigation').length > 0){
    $('nav.navigation').prepend('<button type="button" class="btn-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>');

    $('nav.navigation button.btn-navbar').click(function(){
      if($(this).hasClass('active')){
        $(this).removeClass('active');
        $('nav.navigation').removeClass('active');
      } else {
        $(this).addClass('active');
        $('nav.navigation').addClass('active');
      }
    });
  }

  //Wrap date range in Hemeroteca
  if($('#views-exposed-form-hemeroteca-page').length > 0){
    $('#edit-created-wrapper').addClass('in-group');
    $('#edit-created-1-wrapper').addClass('in-group');
    $('.in-group').wrapAll("<div id='group-created'></div>");
  }

  // First Item of First Page
  if($('.vista-seccion .view-content').length > 0){
    if($('ul.pager').length > 0){
      if($('.pager-previous').length > 0){
        $('.views-row-1').addClass('not-first-page');
        $('.views-row-2').addClass('not-first-page');
        $('.views-row-3').addClass('not-first-page');
        $('.views-row-4').addClass('not-first-page');
        $('.views-row-5').addClass('not-first-page');
        $('.views-row-6').addClass('not-first-page');
        $('.views-row-7').addClass('not-first-page');
        $('.views-row-8').addClass('not-first-page');
        $('#group-top').addClass('not-first-page');
        $('#group-medium').addClass('not-first-page');
        $('#group-bottom').addClass('not-first-page');
        $('#group-extra').addClass('not-first-page');
      } else {
        $('.views-row-1').addClass('first-page');
        $('.views-row-2').addClass('first-page');
        $('.views-row-3').addClass('first-page');
        $('.views-row-4').addClass('first-page');
        $('.views-row-5').addClass('first-page');
        $('.views-row-6').addClass('first-page');
        $('.views-row-7').addClass('first-page');
        $('.views-row-8').addClass('first-page');
        $('#group-top').addClass('first-page');
        $('#group-medium').addClass('first-page');
        $('#group-bottom').addClass('first-page');
        $('#group-extra').addClass('first-page');
      }
    } else {
      $('.views-row-1').addClass('first-page');
      $('.views-row-2').addClass('first-page');
      $('.views-row-3').addClass('first-page');
      $('.views-row-4').addClass('first-page');
      $('.views-row-5').addClass('first-page');
      $('.views-row-6').addClass('first-page');
      $('.views-row-7').addClass('first-page');
      $('.views-row-8').addClass('first-page');
      $('#group-top').addClass('first-page');
      $('#group-medium').addClass('first-page');
      $('#group-bottom').addClass('first-page');
      $('#group-extra').addClass('first-page');
    }

  }

  // Height Box in Posts
  if($('body.context-posts-destacados').length > 0){
    var maxHeight = Math.max.apply(null, $("div.content-post").map(function ()
    {
        return $(this).height();
    }).get());

    $("div.content-post").height(maxHeight+20);
  }

  // Social Icons Position
  // if($('div.field-lugar-fecha-firma').length > 0 && $('#block-service-links-service-links').length > 0){
  //   var offTop = $('div.region-content-inner').offset();
  //   var off = $('div.field-lugar-fecha-firma').offset();

  //   // var offset = (off.top-offTop.top)-104;
  //   var offset = (off.top-offTop.top);

  //   $('#block-service-links-service-links').css('top',offset);
  //   $('#block-print-print-links').css('top',offset);
  // }

  // 3 Columns same height
  if($('.vista-seccion .view-content #group-bottom.first-page').length > 0){
    var h = 0;
    $('.vista-seccion .view-content #group-bottom.first-page .content-entrevista').each(function(i){
      var temp_h = $(this).height();

      if(temp_h > h){h = temp_h}
    })

    $('.vista-seccion .view-content #group-bottom.first-page .content-entrevista').height(h);
  }

  //Crop image first article home and prehome
  $('.view-id-portada .views-row.portada-Noticia .field-op-main-image .field-op-main-image').css('overflow','hidden');
  $('.view-id-preportada .views-row.portada-Noticia .field-op-main-image .field-op-main-image').css('overflow','hidden');

  //Images floated text v-centering - HOME
  var matItems = [
    ".view-portada .views-row-1 .field-body",
    "#myp_portada_sup_izq .views-row-2 .field-body",
    "#myp_portada_sup_izq .views-row-3 .field-body",
    "#myp_portada_med_der .views-row-5 .field-body",
    "#myp_portada_med_der .views-row-6 .field-body",
    "#myp_portada_low_izq .views-row-2 .field-body",
    "#myp_portada_low_izq .views-row-3 .field-body"
  ]
  $.each(matItems, function( index, value ) {
    var text = $(value);
    var image = $(value).parent().find(".field-op-main-image");
    var image2 = $(value).parent().find(".field-op-main-image .field-op-main-image");

    var diff = text.height()-image.height();
    var position = text.position();

    if(text.height()>image.height()){
      image.css("bottom","auto");
      image.css("top",(0))
      image2.css("bottom","auto");
      //image2.css("top",((position.top+(diff/2))-16));
      image2.css("top",((position.top+(diff/2))));
      image2.css("overflow","hidden");
    }
  });

  //Images floated text v-centering - SUBHOME
  var matItems = [
    ".vista-seccion .view-content .views-row-4.first-page"
  ]
  $.each(matItems, function( index, value ) {
    var text = $(value).find(".content-body");
    var image = $(value).find(".views-field-field-op-main-image-1");

    var diff = text.height()-image.height();
    var position = text.position();

    if(text.height()>image.height()){
      image.css("bottom","auto");
      //image.css("top",((position.top+(diff/2))-16));
      image.css("top",((position.top+(diff/2))));
      image.css("overflow","hidden");
    }
  });

  //Image floated text v-centering - SUBHOME views-row-2
  var antetitulo = $('.vista-seccion .view-content .views-row-2.first-page .field-antetitulo');
  var titulo     = $('.vista-seccion .view-content .views-row-2.first-page .content-title');
  var text1      = $('.vista-seccion .view-content .views-row-2.first-page .field-entradilla');
  var text2      = $('.vista-seccion .view-content .views-row-2.first-page .field-body');
  var image      = $('.vista-seccion .view-content .views-row-2.first-page .views-field-field-op-main-image-1');

  image.css("top",(antetitulo.height()+titulo.height())+10);

  if(((text1.height()+text2.height())+100) < image.height()){
    $('.vista-seccion .view-content .views-row-2.first-page .content-entrevista').css("margin-bottom",image.height()-((text1.height()+text2.height())+100)+10);
  } else if((text1.height()+text2.height()) > image.height()){
    image.css("top",(((text1.height()+text2.height())+100)/2)+(antetitulo.height()+titulo.height())+10);
  }


  // #group-bottom empty
  if($("#group-bottom").length >0){
    if(!$.trim($("#group-bottom").html())){
      $("#group-bottom").hide();
    } else {
      //alert("no esta vacio");
    }
  }

  // Print version
  if($(".print-site_name").length >0){
    $("h3.field-label").hide();
    $(".field-destacado").hide();
    $(".field-entrevista").hide();
  }

  // Image Caption
  window.setTimeout("Drupal.imageCaption()",2000);

  //:last-child
  $(':last-child').addClass('lastchild');
};



// DRUPAL BEHAVIORS
// ----------------------------------------------------------------------------------------------------
// Init
Drupal.behaviors.frame = {
  attach: function(context, settings) {
    Drupal.initStart();
    Drupal.manipulateDrupalHTML();
  }
};

})(jQuery);
