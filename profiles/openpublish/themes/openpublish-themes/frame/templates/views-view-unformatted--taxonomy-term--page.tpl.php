<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
$i = 1;
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>

<div id="group-top">
<?php foreach ($rows as $id => $row): 
?>
  <?php if ($i <= 2 ) { ?> 
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php 
       print $row; 
    ?>
  </div>
  <?php } ?>
<?php
  $i++; 
endforeach; 
$i = 1;
?>
</div>

<div id="group-medium">
<?php foreach ($rows as $id => $row): 
?>
  <?php if ($i>=3 && $i<=5 ) { ?> 
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php 
       print $row; 
    ?>
  </div>
  <?php } ?>
<?php
  $i++; 
endforeach; 
$i = 1;
?>
</div>

<div id="group-bottom">
<?php foreach ($rows as $id => $row): 
?>
  <?php if ($i>=6 && $i<=8 ) { ?> 
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php 
       print $row; 
    ?>
  </div>
  <?php } ?>
<?php 
  $i++; 
endforeach; 
$i = 1;
?>
</div>

<div id="group-extra">
<?php foreach ($rows as $id => $row): 
?>
  <?php if ($i >= 9 ) { ?> 
  <div<?php if ($classes_array[$id]) { print ' class="' . $classes_array[$id] .'"';  } ?>>
    <?php 
       print $row; 
    ?>
  </div>
  <?php } ?>
<?php 
  $i++; 
endforeach; 
$i = 1;
?>
</div>

