CREATE TABLE IF NOT EXISTS `t_mypactual` (
  `id` int(11) NOT NULL,
    `fecha` datetime DEFAULT NULL,
	`status` int(1) DEFAULT NULL,
	`seccion` varchar(50) DEFAULT NULL,
	`antetitulo` varchar(500) DEFAULT NULL,
	`titulo` longtext DEFAULT NULL,
	`entradilla` longtext DEFAULT NULL,
	`imagen` varchar(200) DEFAULT NULL,
	`contenido` longtext DEFAULT NULL,
	`anexos` varchar(500) DEFAULT NULL,
	`link` varchar(200) DEFAULT NULL,
	PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) CHARACTER SET utf8
  COLLATE utf8_general_ci;



-- Obtenemos los articulos publicados o solicitados para publicacion
-- if (pendiente='no',0,1)
insert into t_mypactual (id, status)
SELECT distinct componente,  1
FROM t_pasosproduccion
WHERE tipo = 'articulo' AND pendiente = 'no' AND fecharealizado >= '2012-11-08'
order by componente;

-- disable SAFE mode PREFERENCES - SQL Queries
-- actualizamos la seccion y la fecha
update  t_mypactual m2, (select a.id, date(a.fecha) as 'fecha',
						 if (a.tipo='general','Actualidad',
							if (a.tipo='fundaciones','Fundaciones OMC',
							if (a.tipo='sociedades', 'Organizaciones',
							if (a.tipo='web20', 'TIC',
							if (a.tipo='multimedia', 'Multimedia',a.tipo))))) as 'seccion'
						from t_articulos a,t_mypactual m
						where a.id = m.id ) as a2
set m2.fecha = a2.fecha, m2.seccion = a2.seccion
WHERE   m2.id = a2.id;

-- comprobamos las fechas porque hay algunas que se quedan en null
select * from t_mypactual where fecha is null;

update  t_mypactual m2,
(select a.id, date(a.fecha) as 'fecha'
						from t_articulos a,t_mypactual m
						where a.id = m.id ) as a2
set m2.fecha = a2.fecha
WHERE   m2.id = a2.id;

-- fecha es nula en articulo, actualizamos a realizado
select distinct a.*
from t_articulos a,  t_pasosproduccion pp
where pp.componente = a.id
and a.fecha is null;

update  t_mypactual m2, (select distinct m.id, m.fecha, m.titulo, date (pp.fecharealizado) as 'ppfecha'
				from t_mypactual m, t_pasosproduccion pp
				where m.titulo is null
				and pp.componente = m.id
				and pp.pendiente = 'no'
				and m.fecha is null)  as a2
set m2.fecha = a2.ppfecha
WHERE   m2.id = a2.id and m2.fecha is null;

-- comprobamos la seccion
select * from t_mypactual where seccion is null;

-- si la siguiente select da null es pq el registro que dio la seccion nula no existe como articulo
select m.id, m.seccion, a.tipo
from t_articulos a, t_mypactual m
where m.id = a.id
and m.seccion is null;

-- con la siguiente sql se comprobar� que son pruebas o son portada...
select m.id, m.seccion, pp.fecharealizado, pp.tipo
from t_pasosproduccion pp, t_mypactual m
where m.id = pp.componente
and m.seccion is null;

-- se tendr�n que eliminar esos registros
delete from t_mypactual where seccion is null;


-- actualizamos el resto de los campos de la noticia antet�tulo, t�tulo, entradilla, imagen, body, link, anexos...
--
update  t_mypactual m2, (select m.id, m.seccion, a.plantilla, c.espacio, ca.tipo, ca.texto  as 'antetitulo'
						from t_mypactual m,  t_colocacionarticulos c , t_contenidosarticulos ca, t_articulos a
						where c.articulo = m.id
						and a.id= m.id
						and ca.id = c.contenido
						and c.espacio = concat(a.plantilla,'pretitulo')) as a2
set m2.antetitulo = a2.antetitulo
WHERE   m2.id = a2.id;

update  t_mypactual m2, (select m.id, m.seccion, a.plantilla, c.espacio, ca.tipo, ca.texto  as 'titulo'
						from t_mypactual m,  t_colocacionarticulos c , t_contenidosarticulos ca, t_articulos a
						where c.articulo = m.id
						and a.id= m.id
						and ca.id = c.contenido
						and c.espacio = concat(a.plantilla,'titulo')) as a2
set m2.titulo = a2.titulo
WHERE   m2.id = a2.id;


update  t_mypactual m2, (select m.id, m.seccion, a.plantilla, c.espacio, ca.tipo, ca.texto  as 'entradilla'
						from t_mypactual m,  t_colocacionarticulos c , t_contenidosarticulos ca, t_articulos a
						where c.articulo = m.id
						and a.id= m.id
						and ca.id = c.contenido
						and c.espacio = concat(a.plantilla,'contenido1')) as a2
set m2.entradilla = a2.entradilla
WHERE   m2.id = a2.id;


update  t_mypactual m2, (select m.id, m.seccion, a.plantilla, c.espacio, ca.tipo, ca.fichero  as 'imagen'
						from t_mypactual m,  t_colocacionarticulos c , t_contenidosarticulos ca, t_articulos a
						where c.articulo = m.id
						and a.id= m.id
						and ca.id = c.contenido
						and c.espacio = concat(a.plantilla,'imagen1')) as a2
set m2.imagen = a2.imagen
WHERE   m2.id = a2.id;


update  t_mypactual m2, (select m.id, m.seccion, a.plantilla, c.espacio, ca.tipo, ca.texto  as 'body'
						from t_mypactual m,  t_colocacionarticulos c , t_contenidosarticulos ca, t_articulos a
						where c.articulo = m.id
						and a.id= m.id
						and ca.id = c.contenido
						and c.espacio = concat(a.plantilla,'contenido2')) as a2
set m2.contenido = a2.body
WHERE   m2.id = a2.id;

update  t_mypactual m2, (select m.id,m.seccion, ca.link as 'link'
						from t_mypactual m,  t_colocacionarticulos c , t_contenidosarticulos ca
						where c.articulo = m.id
						and ca.id = c.contenido
						and ca.link is not null
						and ca.link <> 'nada') as a2
set m2.link = a2.link
WHERE   m2.id = a2.id;

-- comprobamos contenidos principales que no sean nulos
select * from t_mypactual where titulo is null;

-- puede que sean fotos o videos que no tienen nada asociado y por eso no tienen contenido, pero si es posible que tengan titulo...
select m.*
from t_mypactual m
where m.contenido is null
and link is null
and imagen is null;

-- para la plantilla5 se tiene que hacer un tratamiento especial... pues no se tienen todos los espacios igual que en los otros, esto ocurre con el id=448
-- asi, articulo5contenido1 --> body, noticiaresumencontenido --> entradilla
update t_mypactual
set entradilla = null,  contenido = null
where id = 448;

update  t_mypactual m2, (select ca.texto  as 'body'
						from t_colocacionarticulos c , t_contenidosarticulos ca
						where c.articulo = 448
						and ca.id = c.contenido
						and c.espacio = 'articulo5contenido1') as a2
set m2.contenido = a2.body
WHERE   m2.id = 448;

update  t_mypactual m2, (select ca.texto  as 'entradilla'
						from t_colocacionarticulos c , t_contenidosarticulos ca
						where c.articulo = 448
						and ca.id = c.contenido
						and c.espacio = 'noticiaresumencontenido') as a2
set m2.entradilla = a2.entradilla
WHERE   m2.id = 448;
--


-- los ficheros est�n en F:\OMC\opt\local\apache2\htdocs\periodicocgcom\public_html\nueva\articulos\archivos\imagenes
-- la separacion de los ficheros est� hecha por ";"
update  t_mypactual m2, (SELECT distinct Fa.articulo as 'id',
						group_concat(fa.nombrearchivo separator ',') as 'anexos'
						from t_ficharticulos fa, t_mypactual m
						where fa.articulo = m.id
						group by fa.articulo) as a2
set m2.anexos = a2.anexos
WHERE   m2.id = a2.id;
